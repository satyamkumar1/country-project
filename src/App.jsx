import { useState,createContext } from "react";
import Homepage from "./Components/Homepage";
import Navbar from "./Components/Navbar";
import Serachbar from "./Components/Serachbar";
import Footer from "./Components/Footer";
import Showcartbigger from "./Components/Showcartbigger";
import { Route, Routes } from "react-router-dom";

export const ThemeContext = createContext();
function App() {

  const [isDarkMode, setDarkMode] = useState(false);
 

  const toggleMode = () => {
    setDarkMode(!isDarkMode);
  };



  const [searchData, setSearchData] = useState("");

  function handleinput(data) {
    // console.log(data);
    setSearchData(data);
  }
  const [regiondata, setregiondata] = useState("all");

  function handleregion(data) {
    setregiondata(data);
  }

  const [sorteddata, setsorteddata] = useState("");

  function handlesorteddata(data) {
    setsorteddata(data);
  }

  const [country,setCountry]=useState([]);

  function handlecountrydata(data){
    // console.log("ccc"+data);
    setCountry(data);
  }

  return (
    <>
     <ThemeContext.Provider value={isDarkMode}>
     <Navbar  changevalue={toggleMode}/>
      <Routes>
        <Route
          path="/"
          exact
          element={
            <>
             
              <Serachbar
                handleSearchBar={handleinput}
                handleregion={handleregion}
                handlesortdata={handlesorteddata}
                countrydata={country}
              />
              <Homepage
                name={searchData}
                searchdata={regiondata}
                sorteddata={sorteddata}
                countrydata={handlecountrydata}
              />
              <Footer />
            </>
          }
        ></Route>

        <Route path={`/country/:id`} element={ <>  <Showcartbigger datacount={country} /></> } />
      </Routes>
      </ThemeContext.Provider>
    </>
  );
}

export default App;
