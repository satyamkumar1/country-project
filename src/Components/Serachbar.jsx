import React, { useEffect, useState ,useContext} from "react";
import { ThemeContext } from "../App";

const Serachbar = ({
  handleSearchBar,
  handleregion,
  handlesortdata,
  countrydata,
}) => {
  const [regiondata, setregiondata] = useState("");

  const [subregiondata, setsubregiondata] = useState({});

  const[show,setshow]=useState(false);

const isDarkMode = useContext(ThemeContext);
  

  function getregion(e) {
    handleregion(e.target.value);
    setregiondata(e.target.value);
    if(e.target.value=="all"){
      setshow(false);

    }
    else{
      setshow(true);
    }
  }


   
  useEffect(() => {
  
    const result = countrydata.reduce((acc, item) => {
      if (!acc[item.subregion]) {
        acc[item.subregion] = 0;
      }
      acc[item.subregion] = 0;

      return acc;
    }, {}); 
 
    setsubregiondata(result);
  
  
  }, [countrydata]);

  return (
    <>
    <div className={`head-section ${isDarkMode ? 'search-dark' :'header-light'}`}>
      <div className="header-section">
        <input
          className="input-search"
          type="text"
          placeholder="serach for a country"
          onChange={(e) => handleSearchBar(e.target.value)}
        />

        <select onChange={(e) => getregion(e)}>
        
          <option value={"all"}> Filter by region</option>
          <option value={"Africa"}>Africa</option>
          <option value={"Americas"}>America</option>
          <option value={"Asia"}>Asia</option>
          <option value={"Europe"}>Europe</option>
          <option value={"Oceania"}>Oceania</option>
        </select>

       {show && <select onChange={(e) => getregion(e)}>
       <option> Filter by subregion</option>

          { Object.keys(subregiondata).map((item) => {
                       return <option value={item+1} key={item}>{item}</option>
          })}
        </select>}

        <select onChange={(e) => handlesortdata(e.target.value)}>
        
           <option> sort by</option>        
          <option value={"desc_p"}>decrease by population </option>
          <option value={"inc_p"}>increase by population </option>
          <option value={"desc_a"}>decrease by area </option>
          <option value={"inc_a"}>increase by area </option>
        </select>
      </div>
      </div>
    </>
  );
};

export default Serachbar;
