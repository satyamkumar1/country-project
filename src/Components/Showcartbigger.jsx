import React ,{useContext } from "react";
import { Link, useNavigate ,useParams ,} from "react-router-dom";
import { ThemeContext } from "../App";
import Loader from "./Loader";
const Showcartbigger = ({datacount}) => {


  const navigate = useNavigate();

  function goback(){
 
      // <Navigate to="/" replace={true}/>
      navigate("/");
  }

  const id=useParams()
  
  const isDarkMode = useContext(ThemeContext);


    let data=datacount.filter((item)=>{
      return item.cca3==id.id;
    })
    if(!data.length){
      return(
        <Loader/>
      )
    }

    let allborderCountry=datacount.reduce((ans,item)=>{

        ans[item.cca3]=item.name.common;
      return ans;
    },{})

   
    if(data[0].borders!==undefined){

      var bordercountry=data[0].borders.map((item)=>{

         return  Object.entries(allborderCountry).filter((data)=>{

            if(item==data[0]){
              return true
            }
          })

      })
    }
    

  return (
    <>

        <button className="back-btn" onClick={()=>goback()}> Back</button>
      <div className={`bigger-data ${isDarkMode?"bigger-data-cart":""}`}>
        <img src={data[0].flags.png} className="img" />
        <div className="show-content">
          <h2>{data[0].name.common}</h2>

          <div className={`second-box`}>
            <div className="inner-box1">
            Top Level Domain:{" "}
                    <span>
                      {data[0].tld
                        ? data[0].tld[0]
                        : "N/A"}
                    </span>
            <p>Population:<span>{data[0].population}</span></p>
            <p>Region:<span>{data[0].region}</span></p>
            <p>subregion:<span>{data[0].subregion}</span></p>
            <p>Capital:<span>{data[0].capital}</span></p> 
            </div>

            <div className="inner-box2">
            {/* Native Name:{" "}
                    <span>
                      {data[0].name.nativeName?
                      data[0].name.nativeName.aym.official
                        : "N/A"}
                    </span> */}
            <p>languages:<span>{Object.values(data[0].languages)+","}</span></p>
            <p>currencies:<span>{Object.values(data[0].currencies)[0].name}</span></p>
            <p>area:<span>{data[0].area}sq</span></p>


            </div>

            
          </div>

            <div className="third-box">

              <p>Border Countries: </p>
              <div className="border-area" >
              {data[0].borders===undefined?"N/A ":bordercountry.map((item)=>{
               
                return <Link key={item[0][1]} to={`/country/${item[0][0]}`}> <p  className="border-country" > {
                
                 item [0][1]        
                
                }</p></Link>
              })}
              </div>

            </div>

        </div>
      </div>  
    </>
  );
};

export default Showcartbigger;
