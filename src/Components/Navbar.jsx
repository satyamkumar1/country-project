import React, { useEffect, useState, useContext } from "react";
import "./Style.css";
import { ThemeContext } from "../App";
const Navbar = ({ changevalue }) => {
  const [bgcolor, setBgcolor] = useState("color-white");
  const [btnMode, setbtnMode] = useState("Dark Mode");

  const isDarkMode = useContext(ThemeContext);

  function handlecolor() {
    changevalue();
    if (btnMode == "Dark Mode") {
      document.body.className='color-black';
      setbtnMode("White Mode");
    } else {
      setbtnMode("Dark Mode");
      document.body.className='color-white';

    }
  }

  // function handleColor() {
  //   if (bgcolor == "color-white") {
  //     setBgcolor("color-black");
  //     setbtnMode('White Mode');
  //   } else {
  //     setBgcolor("color-white");
  //     setbtnMode('Dark Mode');
  //   }
  // }

  // useEffect(() => {
  //   document.body.className = bgcolor;
  // }, [bgcolor]);

  return (
    <>
      <div className={`header ${isDarkMode ? "dark" : "navbar-light"}`}>
        <h1>Where in the world?</h1>

        <button className="btn-mode" onClick={handlecolor}>
          <i className="fa-solid fa-moon"></i>
          {btnMode}
        </button>
      </div>
    </>
  );
};

export default Navbar;
