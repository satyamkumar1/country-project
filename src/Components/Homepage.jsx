import React from "react";
import { useEffect, useState,useContext } from "react";
import "./Style.css";
import Showcart from "./Showcart";
import Loader from "./Loader";
import Errorpage from "./Errorpage";
import { ThemeContext } from "../App";
const Homepage = ({ name, searchdata, sorteddata,countrydata}) => {
  const [country, setCountry] = useState([]);
  const [countryname, setCountryname] = useState([]);
  const [loading, setloading] = useState(true);

const isDarkMode = useContext(ThemeContext);



  useEffect(() => {
   
    // document.querySelector(".input-search").value =null;
  
     let url = "https://restcountries.com/v3.1/all";
    
    setloading(true);
    fetch(url)
      .then((data) => {
        setloading(false);

        return data.json();
      })
      .then((item) => {
        setCountry(item);
        setCountryname(item);
        countrydata(item);
        
      });
  }, []);

  useEffect(()=>
  {

   if(searchdata=='all'){
    setCountryname(country);
   }
   else if(searchdata.includes('1')){

      const resultSubregionData = country.filter((oldData ) =>{
        return oldData.subregion===searchdata.slice(0,searchdata.length-1);
      })
    
     setCountryname(resultSubregionData)
   }
   else{
    const resultSubregionData = country.filter((oldData ) =>{
      return oldData.region.includes(searchdata);
    })
  
   setCountryname(resultSubregionData)
   countrydata(resultSubregionData);
   }


  },[searchdata])

  //sort data

  useEffect(() => {

    if (sorteddata == "inc_p") {
      countryname.sort((curr, prev) => {
        return prev.population-curr.population;
      });
    }
    if (sorteddata == "desc_p") {
      countryname.sort((curr, prev) => {
        return curr.population-prev.population
      });
    }

    if (sorteddata == "inc_a") {
      countryname.sort((curr, prev) => {
        return prev.area - curr.area;
      });
    }

    if (sorteddata == "desc_a") {
      countryname.sort((curr, prev) => {
        return curr.area - prev.area;
      });
    }

    setCountryname(countryname);
  }, [sorteddata,countryname]);

  useEffect(() => {
    const contData = countryname.filter((item) => {
     
      const cotName = item.name.common;
      return cotName.toLowerCase().includes(name.toLowerCase());
    });
   
    setCountryname(contData);
  }, [name]);

  return (
    <>
      <div className={`main-box ${isDarkMode ? 'main-box-dark' :'header-light'}`}>
        {loading ? (
          <Loader />
        ) : (
          countryname.map((item) => {
            return <Showcart item={item} key={item.cca3}/>;
          })
        )}

        {countryname.length == 0 && name.length > 0 ? <Errorpage /> : ""}
      </div>
    </>
  );
};

export default Homepage;
