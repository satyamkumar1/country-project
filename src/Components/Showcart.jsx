
import './Style.css';
import { Link } from 'react-router-dom';
import { ThemeContext } from '../App';
import { useContext } from 'react';


const Showcart = ({item ,passdata}) => {

  const isDarkMode = useContext(ThemeContext);
  return (
   <>
   
   
   <div className={`cart ${isDarkMode ? 'cart-area' :'navbar-light'}`}> 
   {/* showing cart */}
   <Link to={`/country/${item.cca3}`}>  <img className='photo'  src={item.flags.png}/> </Link>
             <div className='content-data'>
             <h1>{item.name.common}</h1> 
             <p>Population:{item.population}</p>
             <p>Region:{item.region}</p>
             <p>Capital:{item.capital}</p>
             </div>

             </div>
   
   
   
   </>
  )
}

export default Showcart